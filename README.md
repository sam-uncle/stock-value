# 持仓市值通知系统

#### 1.介绍
本人平时有买一些股票作为理财规划的一部分。但是我不炒短线，属于中长期持有，程序猿没有时间看盘嘛，哈哈。
但是我有时候又想知道自己账户里面股票市值是多少。

不看盘又想了解自己有多少钱，有个人能在收盘后告诉我账户情况岂不是很好，因此就有了本系统。

本系统主要实现如下功能：

每日根据自己的持仓配置，自动计算账户总市值，并邮件通知到指定邮箱。

后续再添加页面配置、图表分析等功能。

#### 2.软件架构

SpringBoot2：脚手架

Mybatis：ORM框架

Jmail：发送邮件

Quartz：处理定时任务

Selenium-java：爬取最新股价

Mysql：不用说了吧

#### 3.安装教程

1. 创建数据库，并执行src/sql目录下的建表语句

2. 修改application-dev.properties配置并重命名为application.properties（或者配置修改后新建application.properties配置文件，并且加上配置spring.profiles.active=dev）

    - 需要配置的点为发件邮箱和密码
    - 数据库配置信息
    - 修改cron表达式，指定为期望的批处理触发时间

3. 根据实际情况对指定表进行配置，配置方法为：

    - T_STOCK_CONFIG：股票信息配置表，用来配置股票编码和名称

    - T_STOCK_HOLDER：持有人信息表，用来配置股票持有人的姓名和邮箱，发邮件时会用到

    - T_STOCK_HOLDER_RELA：持有人账户表，如表名所示用来配置持有人持有了哪些股票

    - T_STOCK_VALUE_DETAIL：账户市值变动明细表，这个表不需要配置，程序自动生成数据，用来静态存储每天的市值情况，后续对总市值添加走势分析等图表分析功能会用到这个表

4. 执行启动类StockValueApplication.java



#### 4.参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request