package com.sam.stockvalue.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class TStockValueDetailExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public TStockValueDetailExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andHolderIdIsNull() {
            addCriterion("HOLDER_ID is null");
            return (Criteria) this;
        }

        public Criteria andHolderIdIsNotNull() {
            addCriterion("HOLDER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andHolderIdEqualTo(Integer value) {
            addCriterion("HOLDER_ID =", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdNotEqualTo(Integer value) {
            addCriterion("HOLDER_ID <>", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdGreaterThan(Integer value) {
            addCriterion("HOLDER_ID >", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("HOLDER_ID >=", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdLessThan(Integer value) {
            addCriterion("HOLDER_ID <", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdLessThanOrEqualTo(Integer value) {
            addCriterion("HOLDER_ID <=", value, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdIn(List<Integer> values) {
            addCriterion("HOLDER_ID in", values, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdNotIn(List<Integer> values) {
            addCriterion("HOLDER_ID not in", values, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdBetween(Integer value1, Integer value2) {
            addCriterion("HOLDER_ID between", value1, value2, "holderId");
            return (Criteria) this;
        }

        public Criteria andHolderIdNotBetween(Integer value1, Integer value2) {
            addCriterion("HOLDER_ID not between", value1, value2, "holderId");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("STOCK_CODE is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("STOCK_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("STOCK_CODE =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("STOCK_CODE <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("STOCK_CODE >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("STOCK_CODE >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("STOCK_CODE <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("STOCK_CODE <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("STOCK_CODE like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("STOCK_CODE not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("STOCK_CODE in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("STOCK_CODE not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("STOCK_CODE between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("STOCK_CODE not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockNumsIsNull() {
            addCriterion("STOCK_NUMS is null");
            return (Criteria) this;
        }

        public Criteria andStockNumsIsNotNull() {
            addCriterion("STOCK_NUMS is not null");
            return (Criteria) this;
        }

        public Criteria andStockNumsEqualTo(Integer value) {
            addCriterion("STOCK_NUMS =", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsNotEqualTo(Integer value) {
            addCriterion("STOCK_NUMS <>", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsGreaterThan(Integer value) {
            addCriterion("STOCK_NUMS >", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsGreaterThanOrEqualTo(Integer value) {
            addCriterion("STOCK_NUMS >=", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsLessThan(Integer value) {
            addCriterion("STOCK_NUMS <", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsLessThanOrEqualTo(Integer value) {
            addCriterion("STOCK_NUMS <=", value, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsIn(List<Integer> values) {
            addCriterion("STOCK_NUMS in", values, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsNotIn(List<Integer> values) {
            addCriterion("STOCK_NUMS not in", values, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsBetween(Integer value1, Integer value2) {
            addCriterion("STOCK_NUMS between", value1, value2, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockNumsNotBetween(Integer value1, Integer value2) {
            addCriterion("STOCK_NUMS not between", value1, value2, "stockNums");
            return (Criteria) this;
        }

        public Criteria andStockPriceIsNull() {
            addCriterion("STOCK_PRICE is null");
            return (Criteria) this;
        }

        public Criteria andStockPriceIsNotNull() {
            addCriterion("STOCK_PRICE is not null");
            return (Criteria) this;
        }

        public Criteria andStockPriceEqualTo(BigDecimal value) {
            addCriterion("STOCK_PRICE =", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceNotEqualTo(BigDecimal value) {
            addCriterion("STOCK_PRICE <>", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceGreaterThan(BigDecimal value) {
            addCriterion("STOCK_PRICE >", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("STOCK_PRICE >=", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceLessThan(BigDecimal value) {
            addCriterion("STOCK_PRICE <", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("STOCK_PRICE <=", value, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceIn(List<BigDecimal> values) {
            addCriterion("STOCK_PRICE in", values, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceNotIn(List<BigDecimal> values) {
            addCriterion("STOCK_PRICE not in", values, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("STOCK_PRICE between", value1, value2, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("STOCK_PRICE not between", value1, value2, "stockPrice");
            return (Criteria) this;
        }

        public Criteria andStockValueIsNull() {
            addCriterion("STOCK_VALUE is null");
            return (Criteria) this;
        }

        public Criteria andStockValueIsNotNull() {
            addCriterion("STOCK_VALUE is not null");
            return (Criteria) this;
        }

        public Criteria andStockValueEqualTo(BigDecimal value) {
            addCriterion("STOCK_VALUE =", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueNotEqualTo(BigDecimal value) {
            addCriterion("STOCK_VALUE <>", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueGreaterThan(BigDecimal value) {
            addCriterion("STOCK_VALUE >", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("STOCK_VALUE >=", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueLessThan(BigDecimal value) {
            addCriterion("STOCK_VALUE <", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("STOCK_VALUE <=", value, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueIn(List<BigDecimal> values) {
            addCriterion("STOCK_VALUE in", values, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueNotIn(List<BigDecimal> values) {
            addCriterion("STOCK_VALUE not in", values, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("STOCK_VALUE between", value1, value2, "stockValue");
            return (Criteria) this;
        }

        public Criteria andStockValueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("STOCK_VALUE not between", value1, value2, "stockValue");
            return (Criteria) this;
        }

        public Criteria andExchangeDateIsNull() {
            addCriterion("EXCHANGE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andExchangeDateIsNotNull() {
            addCriterion("EXCHANGE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeDateEqualTo(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE =", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE <>", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateGreaterThan(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE >", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE >=", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateLessThan(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE <", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("EXCHANGE_DATE <=", value, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateIn(List<Date> values) {
            addCriterionForJDBCDate("EXCHANGE_DATE in", values, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("EXCHANGE_DATE not in", values, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("EXCHANGE_DATE between", value1, value2, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andExchangeDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("EXCHANGE_DATE not between", value1, value2, "exchangeDate");
            return (Criteria) this;
        }

        public Criteria andInsertTimeIsNull() {
            addCriterion("INSERT_TIME is null");
            return (Criteria) this;
        }

        public Criteria andInsertTimeIsNotNull() {
            addCriterion("INSERT_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andInsertTimeEqualTo(Date value) {
            addCriterion("INSERT_TIME =", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeNotEqualTo(Date value) {
            addCriterion("INSERT_TIME <>", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeGreaterThan(Date value) {
            addCriterion("INSERT_TIME >", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("INSERT_TIME >=", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeLessThan(Date value) {
            addCriterion("INSERT_TIME <", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeLessThanOrEqualTo(Date value) {
            addCriterion("INSERT_TIME <=", value, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeIn(List<Date> values) {
            addCriterion("INSERT_TIME in", values, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeNotIn(List<Date> values) {
            addCriterion("INSERT_TIME not in", values, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeBetween(Date value1, Date value2) {
            addCriterion("INSERT_TIME between", value1, value2, "insertTime");
            return (Criteria) this;
        }

        public Criteria andInsertTimeNotBetween(Date value1, Date value2) {
            addCriterion("INSERT_TIME not between", value1, value2, "insertTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("UPDATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("UPDATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("UPDATE_TIME =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("UPDATE_TIME <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("UPDATE_TIME >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATE_TIME >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("UPDATE_TIME <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("UPDATE_TIME <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("UPDATE_TIME in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("UPDATE_TIME not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("UPDATE_TIME between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("UPDATE_TIME not between", value1, value2, "updateTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}