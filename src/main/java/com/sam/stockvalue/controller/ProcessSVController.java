package com.sam.stockvalue.controller;

import com.sam.stockvalue.service.ProcessSVService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Controller;

/**
 * @description:
 * @author:
 */
@Controller
public class ProcessSVController extends QuartzJobBean {
    Logger logger = LoggerFactory.getLogger(ProcessSVController.class);
    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        ApplicationContext applicationContext = null;
        try {
            applicationContext = (ApplicationContext)context.getScheduler().getContext().get("applicationContext");
            applicationContext.getBean(ProcessSVService.class).doBusiness();

        } catch (SchedulerException e) {
            logger.error("系统异常：[{}]", e.getStackTrace());
        }
    }
}
