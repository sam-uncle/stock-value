package com.sam.stockvalue.dao;

import com.sam.stockvalue.model.TStockHolderRela;

import java.util.List;

/**
 * @description:
 * @author:
 */
public interface TStockHolderRelaDao {


    List<TStockHolderRela> getStockHolerRela(TStockHolderRela record);
}
