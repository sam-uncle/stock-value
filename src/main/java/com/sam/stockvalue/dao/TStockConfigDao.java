package com.sam.stockvalue.dao;

import com.sam.stockvalue.model.TStockConfig;

import java.util.List;

/**
 * @description:
 * @author:
 */
public interface TStockConfigDao {

    List<TStockConfig> getAllStockConfig();

    int updateByPrimaryKeySelective(TStockConfig record);
}
