package com.sam.stockvalue.dao.impl;

import com.alibaba.druid.util.StringUtils;
import com.sam.stockvalue.dao.TStockValueDetailDao;
import com.sam.stockvalue.mappers.TStockValueDetailMapper;
import com.sam.stockvalue.model.TStockValueDetail;
import com.sam.stockvalue.model.TStockValueDetailExample;
import com.sam.stockvalue.model.TStockValueDetailExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author:
 */
@Repository
public class TStockValueDetailDaoImpl implements TStockValueDetailDao {

    @Autowired
    TStockValueDetailMapper mapper;

    @Override
    public int insertSelective(TStockValueDetail record) {
        record.setInsertTime(new Date());
        record.setUpdateTime(new Date());
        return mapper.insertSelective(record);
    }

    @Override
    public int deleteStockValueDetail(TStockValueDetail record) {
        TStockValueDetailExample example = new TStockValueDetailExample();
        Criteria criteria = example.createCriteria();
        if(record.getHolderId() != null){
            criteria.andHolderIdEqualTo(record.getHolderId());
        }

        if(record.getExchangeDate() != null) {
            criteria.andExchangeDateEqualTo(record.getExchangeDate());
        }
        return mapper.deleteByExample(example);
    }

    @Override
    public List<TStockValueDetail> getStockValueDetail(TStockValueDetail record) {
        TStockValueDetailExample example = new TStockValueDetailExample();
        Criteria criteria = example.createCriteria();
        if(record.getHolderId() != null){
            criteria.andHolderIdEqualTo(record.getHolderId());
        }

        if(!StringUtils.isEmpty(record.getStockCode())) {
            criteria.andStockCodeEqualTo(record.getStockCode());
        }
        if(record.getExchangeDate() != null) {
            criteria.andExchangeDateEqualTo(record.getExchangeDate());
        }
        return mapper.selectByExample(example);
    }
}
