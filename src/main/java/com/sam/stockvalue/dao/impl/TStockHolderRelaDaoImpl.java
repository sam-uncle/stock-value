package com.sam.stockvalue.dao.impl;

import com.sam.stockvalue.dao.TStockHolderRelaDao;
import com.sam.stockvalue.mappers.TStockHolderRelaMapper;
import com.sam.stockvalue.model.TStockHolderRela;
import com.sam.stockvalue.model.TStockHolderRelaExample;
import com.sam.stockvalue.model.TStockHolderRelaExample.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author:
 */
@Repository
public class TStockHolderRelaDaoImpl implements TStockHolderRelaDao {

    @Autowired
    TStockHolderRelaMapper mapper;

    @Override
    public List<TStockHolderRela> getStockHolerRela(TStockHolderRela record) {
        TStockHolderRelaExample example = new TStockHolderRelaExample();
        Criteria criteria = example.createCriteria();
        criteria.andHolderIdEqualTo(record.getHolderId());
        return mapper.selectByExample(example);
    }
}
