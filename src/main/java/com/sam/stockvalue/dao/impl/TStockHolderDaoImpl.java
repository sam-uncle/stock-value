package com.sam.stockvalue.dao.impl;

import com.sam.stockvalue.dao.TStockHolderDao;
import com.sam.stockvalue.mappers.TStockHolderMapper;
import com.sam.stockvalue.model.TStockHolder;
import com.sam.stockvalue.model.TStockHolderExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description:
 * @author:
 */
@Repository
public class TStockHolderDaoImpl implements TStockHolderDao {

    @Autowired
    TStockHolderMapper mapper;

    @Override
    public List<TStockHolder> getAllStockHolder() {
        TStockHolderExample example = new TStockHolderExample();
        return mapper.selectByExample(example);
    }
}
