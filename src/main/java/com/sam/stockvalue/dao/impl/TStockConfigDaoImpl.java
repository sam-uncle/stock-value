package com.sam.stockvalue.dao.impl;

import com.sam.stockvalue.dao.TStockConfigDao;
import com.sam.stockvalue.mappers.TStockConfigMapper;
import com.sam.stockvalue.model.TStockConfig;
import com.sam.stockvalue.model.TStockConfigExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * @description:
 * @author:
 */
@Repository
public class TStockConfigDaoImpl implements TStockConfigDao {

    @Autowired
    TStockConfigMapper mapper;

    @Override
    public int updateByPrimaryKeySelective(TStockConfig record) {
        record.setUpdateTime(new Date());
        return mapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<TStockConfig> getAllStockConfig() {
        TStockConfigExample example = new TStockConfigExample();
        return mapper.selectByExample(example);
    }
}
