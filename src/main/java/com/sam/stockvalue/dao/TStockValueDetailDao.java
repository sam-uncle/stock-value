package com.sam.stockvalue.dao;

import com.sam.stockvalue.model.TStockValueDetail;

import java.util.List;

/**
 * @description:
 * @author:
 */
public interface TStockValueDetailDao {

    int insertSelective(TStockValueDetail record);

    List<TStockValueDetail> getStockValueDetail(TStockValueDetail record);

    int deleteStockValueDetail(TStockValueDetail record);
}
