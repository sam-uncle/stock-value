package com.sam.stockvalue.dao;

import com.sam.stockvalue.model.TStockHolder;

import java.util.List;

/**
 * @description:
 * @author:
 */
public interface TStockHolderDao {

    List<TStockHolder> getAllStockHolder();
}
