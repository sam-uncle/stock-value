package com.sam.stockvalue.service;

/**
 * @description:
 * @author:
 */
public interface MailSendService {

    public void sendSimpleMail(String context);

    public void sendSimpleMail(String to, String context);

    public void sendSimpleMail(String to, String subject, String context);

    /**
     * 发送HTML邮件
     */
    public void sendMimeMail(String to, String subject, String context);

    /**
     * 发送带附件的邮件
     */
    public void sendAttachMail(String[] to, String subject, String context, String filePath);

    /**
     * 发送正文带图片的邮件
     */
    public void sendInlineMail(String to, String subject, String context, String filePath, String resId);
}
