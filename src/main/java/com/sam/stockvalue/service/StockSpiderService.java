package com.sam.stockvalue.service;

import com.sam.stockvalue.model.TStockConfig;

/**
 * @description:
 * @author:
 */
public interface StockSpiderService {

    void doSpiderStock(TStockConfig stockConfig);

    void clearThreadLocal();
}