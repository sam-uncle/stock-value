package com.sam.stockvalue.mappers;

import com.sam.stockvalue.model.TStockValueDetail;
import com.sam.stockvalue.model.TStockValueDetailExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TStockValueDetailMapper {
    int countByExample(TStockValueDetailExample example);

    int deleteByExample(TStockValueDetailExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TStockValueDetail record);

    int insertSelective(TStockValueDetail record);

    List<TStockValueDetail> selectByExample(TStockValueDetailExample example);

    TStockValueDetail selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TStockValueDetail record, @Param("example") TStockValueDetailExample example);

    int updateByExample(@Param("record") TStockValueDetail record, @Param("example") TStockValueDetailExample example);

    int updateByPrimaryKeySelective(TStockValueDetail record);

    int updateByPrimaryKey(TStockValueDetail record);
}