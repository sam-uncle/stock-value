package com.sam.stockvalue.mappers;

import com.sam.stockvalue.model.TStockConfig;
import com.sam.stockvalue.model.TStockConfigExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TStockConfigMapper {
    int countByExample(TStockConfigExample example);

    int deleteByExample(TStockConfigExample example);

    int deleteByPrimaryKey(String code);

    int insert(TStockConfig record);

    int insertSelective(TStockConfig record);

    List<TStockConfig> selectByExample(TStockConfigExample example);

    TStockConfig selectByPrimaryKey(String code);

    int updateByExampleSelective(@Param("record") TStockConfig record, @Param("example") TStockConfigExample example);

    int updateByExample(@Param("record") TStockConfig record, @Param("example") TStockConfigExample example);

    int updateByPrimaryKeySelective(TStockConfig record);

    int updateByPrimaryKey(TStockConfig record);
}