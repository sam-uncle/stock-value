package com.sam.stockvalue.mappers;

import com.sam.stockvalue.model.TStockHolder;
import com.sam.stockvalue.model.TStockHolderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TStockHolderMapper {
    int countByExample(TStockHolderExample example);

    int deleteByExample(TStockHolderExample example);

    int deleteByPrimaryKey(Integer holderId);

    int insert(TStockHolder record);

    int insertSelective(TStockHolder record);

    List<TStockHolder> selectByExample(TStockHolderExample example);

    TStockHolder selectByPrimaryKey(Integer holderId);

    int updateByExampleSelective(@Param("record") TStockHolder record, @Param("example") TStockHolderExample example);

    int updateByExample(@Param("record") TStockHolder record, @Param("example") TStockHolderExample example);

    int updateByPrimaryKeySelective(TStockHolder record);

    int updateByPrimaryKey(TStockHolder record);
}