package com.sam.stockvalue.mappers;

import com.sam.stockvalue.model.TStockHolderRela;
import com.sam.stockvalue.model.TStockHolderRelaExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface TStockHolderRelaMapper {
    int countByExample(TStockHolderRelaExample example);

    int deleteByExample(TStockHolderRelaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(TStockHolderRela record);

    int insertSelective(TStockHolderRela record);

    List<TStockHolderRela> selectByExample(TStockHolderRelaExample example);

    TStockHolderRela selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") TStockHolderRela record, @Param("example") TStockHolderRelaExample example);

    int updateByExample(@Param("record") TStockHolderRela record, @Param("example") TStockHolderRelaExample example);

    int updateByPrimaryKeySelective(TStockHolderRela record);

    int updateByPrimaryKey(TStockHolderRela record);
}