package com.sam.stockvalue.utils;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {


	public static Date addDays(Date date, int amount) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DAY_OF_MONTH, amount);
		return calendar.getTime();
	}
	
}
