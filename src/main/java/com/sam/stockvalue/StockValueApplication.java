package com.sam.stockvalue;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@MapperScan(value = "com.sam.stockvalue.mappers")
public class StockValueApplication {

    public static void main(String[] args) {

        SpringApplication.run(StockValueApplication.class, args);
    }

}
